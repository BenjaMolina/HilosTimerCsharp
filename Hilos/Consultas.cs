﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hilos
{
    public class Consultas
    {
        MySqlConnection cnn;
        DataTable dt;
        MySqlCommand cmd;
        MySqlDataAdapter da;
        public string insertar(string micadena)
        {
            string Respuesta = "";
            cnn = new MySqlConnection();

            try
            {
                cnn.ConnectionString = conexion.cadena;
                cnn.Open(); //Me quedé aquí, iba a insertar un nuevo proveedor, regresar a NProveedores después


                cmd = new MySqlCommand();
                string sql = micadena;

                cmd.CommandText = sql;

                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();
                Respuesta = "Garatía agregada exitosamente";
            }

            catch (Exception ex)
            {
                Respuesta = "Error al intentar insertar en garantía " + ex.Message;
            }

            finally
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

            //return Respuesta;
            return Respuesta;
        }

        public string actualizar(string micadena)
        {
            string Respuesta = "";
            cnn = new MySqlConnection();

            try
            {
                cnn.ConnectionString = conexion.cadena;
                cnn.Open(); //Me quedé aquí, iba a insertar un nuevo proveedor, regresar a NProveedores después


                cmd = new MySqlCommand();
                string sql = micadena;

                cmd.CommandText = sql;

                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();
                Respuesta = "registro actualizado exitosamente";
            }

            catch (Exception ex)
            {
                Respuesta = "Error al intentar actualizar en garantía " + ex.Message;
            }

            finally
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

            //return Respuesta;
            return Respuesta;
        }

        public DataTable consulta(string cadena)
        {
            //Con capas
            dt = new DataTable();
            cnn = new MySqlConnection();

            try
            {
                cnn.ConnectionString = conexion.cadena;
                cnn.Open();

                cmd = new MySqlCommand();
                da = new MySqlDataAdapter(cadena, cnn);
                da.Fill(dt); //Llenar data table                
            }

            catch (Exception ex)
            {
                dt = null;
                throw new Exception("Error al intentar ejecutar el procedimiento almacenado Produccion.ListaProductos. " + ex.Message, ex);
            }

            finally
            {
                cnn.Close();
            }

            return dt;
        }
    }
}
