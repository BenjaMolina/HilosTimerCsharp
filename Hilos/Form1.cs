﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hilos
{
    public partial class Form1 : Form
    {
        conexion conecta = new conexion();
        Consultas consulta = new Consultas();
        //Tiempo en min
        public List<Tiempo> miLista;
        public DataTable miDT;
        System.Timers.Timer timer = new System.Timers.Timer(1000);
            

        public int tiempoActual = 0;
        public Form1()
        {
            InitializeComponent();

            var tiempo1 = new Tiempo() { cantidad = 20,tipo = "INSERCION", descripcion = "INSERT INTO pruebas(campo1, campo2, campo3) VALUES('prueba1', 'prueba2', '"+button1.Text+"')" };
            var tiempo2 = new Tiempo() { cantidad =5, tipo = "UPDATE", descripcion = "UPDATE pruebas SET campo1 = '"+button1.Text+"' WHERE idpruebas = 5" };
            var tiempo3 = new Tiempo() { cantidad = 11, tipo = "CONSULTA", descripcion = "SELECT * FROM pruebas" };
            miLista = new List<Tiempo>() { tiempo1, tiempo2, tiempo3 };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(conecta.ChequearConexion());
            //timer1.Start();
            
            Console.WriteLine("Primera vez");
            // Hook up the Elapsed event for the timer.
            timer.Elapsed += tickea;

            timer.Enabled = true;
        }

        public void tickea(object sender, EventArgs e)
        {
            tiempoActual = tiempoActual + 1;
            Console.WriteLine(tiempoActual);
            foreach (var item in miLista)
            {
                if (tiempoActual == item.cantidad || tiempoActual % item.cantidad == 0)
                {
                    if (item.tipo == "INSERCION")
                    {
                        timer.Stop();
                        //consulta.insertar(item.descripcion);
                        timer.Start();
                        Console.WriteLine("INSERTAaaaaaaa");
                    }
                    else if (item.tipo == "UPDATE")
                    {
                        timer.Stop();
                        Thread.Sleep(400);
                        timer.Start();
                        Console.WriteLine("Actualiza");
                        //consulta.actualizar(item.descripcion);
                    }
                    else if (item.tipo == "CONSULTA")
                    {
                        timer.Stop();
                        //miDT = consulta.consulta(item.descripcion);
                        timer.Start();
                        //dataGridView1.DataSource = miDT;
                        Console.WriteLine(miDT.Rows[4]["campo2"].ToString());
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tiempoActual = tiempoActual + 1;
            foreach (var item in miLista)
            {
                if (tiempoActual == item.cantidad || tiempoActual % item.cantidad == 0)
                {
                    if (item.tipo == "INSERCION")
                        consulta.insertar(item.descripcion);
                    else if (item.tipo == "UPDATE")
                    {

                        Thread.Sleep(50000);
                        //consulta.actualizar(item.descripcion);
                    }
                    else if (item.tipo == "CONSULTA")
                    {
                        miDT = consulta.consulta(item.descripcion);

                        //dataGridView1.DataSource = miDT;
                        Console.WriteLine(miDT.Rows[4]["campo2"].ToString());
                    }
                }
            }
        }

    }
}
